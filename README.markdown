# Troy-Bilt 2013 Page Redesigns

## Template Preview Links:

- [Products List](http://troy2013.herokuapp.com/products)
- [Products List w/ Filters](http://troy2013.herokuapp.com/products?filter-price=500_1000&filter-thrower_type=two_stage)
- [Products Compare](http://troy2013.herokuapp.com/products/compare?product_1=flurry-1400&product_2=squall-210)
- [Products Detail](http://troy2013.herokuapp.com/products/detail)
- [Landing Page 1](http://troy2013.herokuapp.com/promo/1)
- [Landing Page 2](http://troy2013.herokuapp.com/promo/2)
- [Landing Page 2 - Custom Layout](http://troy2013.herokuapp.com/promo/2?layout=minimal)
- [Landing Page 3](http://troy2013.herokuapp.com/promo/3)
- [Landing Page 3 - Tall Banner](http://troy2013.herokuapp.com/promo/3?banner=tall)
- [Landing Page 3 - Banner with Actions](http://troy2013.herokuapp.com/promo/3?banner=with-actions)

### 2-3 Column Parts and Manuals
- [Parts & Manual Finder](http://troy2013.herokuapp.com/owners-center/parts-manual-finder)
- [Parts Finder](http://troy2013.herokuapp.com/owners-center/parts-finder)
- [Operator's Manuals](http://troy2013.herokuapp.com/owners-center/operators-manuals)
- [Parts Finder Search Results](http://troy2013.herokuapp.com/owners-center/parts-finder-results?part_number=12345)
- [Operator's Manual Search Results](http://troy2013.herokuapp.com/owners-center/operators-manuals-results?model_number=12345&serial_number=)
- [Parts List](http://troy2013.herokuapp.com/parts)
- [Parts List w/ Filters](http://troy2013.herokuapp.com/parts?filter-price=500_1000&filter-thrower_type=two_stage)

### Locate Your Model and Serial Numbers & Mega Menu Updates
- [Locating Your Model and Serial Numbers](http://troy2013.herokuapp.com/owners-center/locating-your-model-and-serial-number)

### Owner's Center Changes
- [Owner's Center Landing](http://troy2013.herokuapp.com/owners-center)
- [Owner's Center - Help](http://troy2013.herokuapp.com/owners-center/help)

### Content Pages
- [Tips, Advice & How-To’s Landing](http://troy2013.herokuapp.com/tips-advice-how-tos)
- [Tips, Advice & How-To’s Landing - Lawn Care](http://troy2013.herokuapp.com/tips-advice-how-tos/lawn-care)
- [Tips, Advice & How-To’s Landing - Gardening](http://troy2013.herokuapp.com/tips-advice-how-tos/gardening)
- [Tips, Advice & How-To’s Landing - Trees & Leaves](http://troy2013.herokuapp.com/tips-advice-how-tos/trees-leaves)
- [Tips, Advice & How-To’s Landing - Snow](http://troy2013.herokuapp.com/tips-advice-how-tos/snow)
- [Meet the Saturday6](http://troy2013.herokuapp.com/tips-advice-how-tos/meet-the-saturday6)
- [The Dirt Archive](http://troy2013.herokuapp.com/tips-advice-how-tos/the-dirt)
- [Tips, Advice & How-To’s Landing - Article Detail, Option A](http://troy2013.herokuapp.com/tips-advice-how-tos/article-a)
- [Tips, Advice & How-To’s Landing - Article Detail, Option B](http://troy2013.herokuapp.com/tips-advice-how-tos/article-b)
- [Tips, Advice & How-To’s Landing - Video Detail](http://troy2013.herokuapp.com/tips-advice-how-tos/video-detail)

### Education Pages
- [Education Category](http://troy2013.herokuapp.com/education/lawn-care)
- [Education Sub-Category](http://troy2013.herokuapp.com/education/lawn-care/lawn-mowers)
