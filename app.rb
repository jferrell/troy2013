require 'sinatra'
require 'json'
require_relative 'models/product'

configure do
  enable :sessions
  set :session_secret, 'b0sco'
  disable :protection
  if ENV['RACK_ENV'] != "development"
    set :static_cache_control, [:public, :max_age => 31536000]
  end
end

before do
  @serve_minified = false
  if ENV['RACK_ENV'] != "development"
    expires 3000, :public, :must_revalidate
    @serve_minified = true
  end
end

get '/' do
  erb :index, :layout => :layout
end

get '/owners-center' do
  erb :owners_center, :layout => :layout
end

get '/owners-center/help' do
  erb :owners_center_help, :layout => :layout
end

get '/owners-center/parts-manual-finder' do
  erb :parts_manual_finder, :layout => :layout
end

get '/owners-center/parts-finder' do
  erb :parts_finder, :layout => :layout
end

get '/owners-center/operators-manuals' do
  erb :manual_finder, :layout => :layout
end

get '/owners-center/parts-finder-results' do
  if params['part_number']
    @query = params['part_number'] 
  elsif params['model_number']
    @query = params['model_number']
  else
    @query = ''
  end
  erb :parts_finder_results, :layout => :layout
end

get '/owners-center/operators-manuals-results' do
  if params['model_number']
    @query = params['model_number']
  elsif params['serial_number']
    @query = params['serial_number']
  else
    @query = ''
  end
  erb :operators_manuals_results, :layout => :layout
end

get '/owners-center/locating-your-model-and-serial-number' do
  erb :locating_model, :layout => :layout
end

get '/promo/:id' do
  if params['layout'] && params['layout'] == 'minimal'
    @layout = :minimal_layout
  else
    @layout = :layout
  end
  erb "landing_page_#{params[:id]}".to_sym, :layout => @layout
end

get '/parts' do
  if params[:filter]
    @products = Product.get_sample.to_a.sample(3)
  elsif params[:price] && params['price'] == 'low'
    @products = Product.get_sample.sort_by { |id, product| product["price"] }
  elsif params[:price] && params['price'] == 'high'
    @products = Product.get_sample.sort_by { |id, product| product["price"] }.reverse
  else
    @products = Product.get_sample
  end
  erb :parts_list, :layout => :layout
end

get '/products' do
  if params[:filter]
    @products = Product.get_sample.to_a.sample(3)
  elsif params[:price] && params['price'] == 'low'
    @products = Product.get_sample.sort_by { |id, product| product["price"] }
  elsif params[:price] && params['price'] == 'high'
    @products = Product.get_sample.sort_by { |id, product| product["price"] }.reverse
  else
    @products = Product.get_sample
  end
  erb :products_list, :layout => :layout
end

get '/products/quick-view' do
  erb :product_modal, :layout => :modal
end

get '/products/detail' do
  erb :products_detail, :layout => :layout
end

get '/products/compare' do
  erb :products_compare, :layout => :layout
end

get '/tips-advice-how-tos' do
  erb :tips_advice_how_tos, :layout => :layout
end

get '/tips-advice-how-tos/lawn-care' do
  erb :tips_advice_lawn_care, :layout => :layout
end

get '/tips-advice-how-tos/gardening' do
  erb :tips_advice_gardening, :layout => :layout
end

get '/tips-advice-how-tos/trees-leaves' do
  erb :tips_advice_trees_leaves, :layout => :layout
end

get '/tips-advice-how-tos/snow' do
  erb :tips_advice_snow, :layout => :layout
end

get '/tips-advice-how-tos/meet-the-saturday6' do
  erb :meet_the_saturday6, :layout => :layout
end

get '/tips-advice-how-tos/the-dirt' do
  erb :the_dirt, :layout => :layout
end

get '/share-bar' do
  erb :social_bar, :layout => :layout
end

get '/tips-advice-how-tos/article-a' do
  erb :tips_advice_article_a, :layout => :layout
end

get '/tips-advice-how-tos/article-b' do
  erb :tips_advice_article_b, :layout => :layout
end

get '/tips-advice-how-tos/video-detail' do
  erb :tips_advice_video_detail, :layout => :layout
end

post '/cart-calculator' do
  erb :cart_calculator, :layout => false
end

get '/email-friend' do
  erb :email_friend_form, :layout => false
end

post '/email-friend' do
  @success = true
  erb :email_friend_form, :layout => false
end

get '/email-template' do
  erb :email_template, :layout => false
end

get '/education/:category' do
  @category = params[:category]
  erb :education_category, :layout => :layout
end

get '/education/:category/:subcategory' do
  @category = params[:category]
  @subcategory = params[:subcategory]
  erb :education_subcategory, :layout => :layout
end

# Canada Templates
get '/canada' do
  erb :index, :layout => :canada_layout
end

get '/canada/products/registration' do
  @canada = true
  erb :'canada/products_registration', :layout => :canada_layout
end

get '/canada/products/registration/survey' do
  @canada = true
  erb :'canada/products_registration_survey', :layout => :canada_layout
end

get '/canada/user-registration' do
  @canada = true
  erb :'canada/user_registration', :layout => :canada_layout
end

helpers do

  def capitalize_all_words(str)
    str.gsub(/-/,' ').split.map(&:capitalize).join(' ')
  end

end
