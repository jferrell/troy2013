var ProductFinder = {};
//set the first slot to 1
ProductFinder.current_slot = 1;
//tell app where to get first set of data
ProductFinder.start_url = '/product-finder/content/slot-1.json';
//make setup state false
ProductFinder.setup = false;
//create array for slot data
ProductFinder.slot_data = [];
//container for load spinner
ProductFinder.spinner_container = document.createElement('div');

$(function () {
	ProductFinder.productSelector();
	ProductFinder.videoHandlers();
	$('div.carousel div.carousel-content').jCarouselLite({
	    btnNext: ".carousel .next",
	    btnPrev: ".carousel .prev"
	});
});

//load spinner
$.fn.spin = function(opts) {
  this.each(function() {
    var $this = $(this),
        data = $this.data();

    if (data.spinner) {
      data.spinner.stop();
      delete data.spinner;
    }
    if (opts !== false) {
      data.spinner = new Spinner($.extend({color: $this.css('color')}, opts)).spin(this);
    }
  });
  return this;
};

ProductFinder.videoHandlers = function () {
	//only use listeners when modal is open and there is a player window
	$(document).bind('cbox_complete', function () {
		if($('#video-player').size() > 0) {
			$('a.play-video').click(function () {
				var vid_id = ProductFinder.extractVidID($(this).attr('href'));
				ProductFinder.videoCue(true, vid_id );
				return false;
			});
		}
	});
}

ProductFinder.videoModal = function () {
	var modal_height;
	//to fix weird height issue in ie7
	var ie7 = (navigator.appVersion.indexOf("MSIE 7.") != -1) ? true : false;
	ie7 ? modal_height = '490px' : modal_height = '455px';
	$('.teaser a.video').colorbox({ width: '810px', height: modal_height });
}

ProductFinder.videoCue = function (autoplay, start_vid) {
	$('.vid-link:first a').addClass('playing');
	$("#video-player").empty();
	$("#video-player").tubeplayer({
    	width: '530px', 
    	height: '320px',
    	allowFullScreen: "true",
    	initialVideo: start_vid,
    	preferredQuality: "default",
    	autoPlay: autoplay
    });
}

ProductFinder.extractVidID = function (vid_link) {
	//the two most common ways people like to YouTube Videos
	var youtube_pattern_1 = /^.+v=(.[^&]+).*$/
	var youtube_pattern_2 = /^.+youtu\.be\/(.+)$/
	return vid_link.replace(youtube_pattern_1,'$1').replace(youtube_pattern_2,'$1');
}

ProductFinder.evenColumns = function (cols) {
	var tallest = 0;
	$(cols).each(function (i) {
		$(this).height() > tallest ? tallest = $(this).height() : '';
		i == $(cols).size() - 1 ? $(this).addClass('last') : '';
	});	
	$(cols).css('min-height',tallest);
}

ProductFinder.productSelector = function () {
	//make sure it exists before firing all this fancy code
	if($('#selector-widget').size() > 0) {
		//setup the vars
		ProductFinder.all_checked = 3;
		ProductFinder.num_checked = 0;
		
		ProductFinder.cancel_btn = $('#selector-widget a.cancel');
		ProductFinder.back_btn = $('#selector-widget a.back');
		ProductFinder.submit_btn = $('#selector-widget input.submit');
		//create the checked image
		ProductFinder.checked = document.createElement('img');
		$(ProductFinder.checked).attr({'src':'/wcpics/TroyBiltUSUS/en_US/images/product-finder/selected.png','class':'selected'});
		//hide the action buttons
		$(ProductFinder.cancel_btn).css('visibility','hidden');
		$(ProductFinder.back_btn).css('visibility','hidden');
		$(ProductFinder.submit_btn).css('visibility','hidden');
		$('#selector-widget').addClass('has-' + ProductFinder.all_checked + '-slots');
		//fetch the first set of data
		ProductFinder.getSelectorData();
	}
}

ProductFinder.getSelectorData = function () {	
	ProductFinder.loadSpinner($('#product-finder-selector'), "start");
	$.ajax({
		//default url is set at the top other urls come from json data
		url: (!ProductFinder.setup ? ProductFinder.start_url : ProductFinder.slot_data[ProductFinder.current_slot - 2].submit_url),
		//first time no data is sent. other requests send the answer in order to retrieve next dataset.
		data: { answer: (!ProductFinder.setup ? null : $('input.customize-option:eq(' + (ProductFinder.current_slot - 2) + ')').val() ) },
		dataType: 'json',
		success: function(data) {
			ProductFinder.slot_data.push(data);
			//build the form markup
			$(ProductFinder.slot_data).each(function (i) {
				this.id = i + 1;
				$(this.answers).each(function (j) {
					this.id = j + 1;
				});
			});
			var template_render = _.template($( "#selector-template" ).html());
			$('#selector-widget fieldset').append(template_render( ProductFinder.slot_data[ProductFinder.current_slot - 1] ));
			ProductFinder.evenColumns($('#selector-widget div[id^="cat-"]'));
			$('#selector-widget span.nav:eq(' + (ProductFinder.current_slot - 1) + ') a:first').addClass('selected');
			$('#selector-widget div:eq(' + (ProductFinder.current_slot - 1) + ') span.img-caption').css('background-image', 'url(' + ProductFinder.slot_data[ProductFinder.current_slot - 1].answers[0].image + ')');
			if(!ProductFinder.setup) {
				ProductFinder.productSelectorHandlers();
			}
			$('#selector-widget div:eq(' + (ProductFinder.current_slot - 1) + ')').append(ProductFinder.back_btn);
			ProductFinder.loadSpinner($('#product-finder-selector'), "stop");
		}
	});
}

ProductFinder.productSelectorHandlers = function () {
	ProductFinder.setup = true;
	//swap the image on mouseover
	$('#selector-widget span.nav a').live('mouseover', function () {
		if(!$(this).closest('div').hasClass('clicked')) {
			//to shift the bg position for the sprites
			var selected_index = parseInt($(this).attr('href').replace(/^.+([0-9]+)$/, '$1')) - 1;
			$(this).closest('div').find('span.img-caption').css('background-image', 'url(' + ProductFinder.slot_data[ProductFinder.current_slot - 1].answers[selected_index].image + ')');
			$(this).closest('div').find('span.nav a').removeClass('selected');
			$(this).addClass('selected');
			$(this).closest('div').attr('class','');
			$(this).closest('div').addClass($(this).attr('id'));
		}
	});
	//lock in the values
	$('#selector-widget span.nav a').live('click', function () {
		if(!$(this).closest('div').hasClass('clicked')) {
			$(ProductFinder.back_btn).css('visibility','visible');
			$(ProductFinder.cancel_btn).css('visibility','visible');
			$(this).closest('div').addClass('clicked').append($(ProductFinder.checked).clone()).find('input.customize-option').val($(this).text());
			ProductFinder.num_checked++;
			ProductFinder.current_slot++;
			if(ProductFinder.num_checked == ProductFinder.all_checked)
			{
				$(ProductFinder.submit_btn).css('visibility','visible');
			}
			else
			{
				ProductFinder.getSelectorData();
			}
		}
		return false;
	});
	//go back one step
	$(ProductFinder.back_btn).live('click', function(){
		ProductFinder.current_slot--;
		ProductFinder.num_checked--;
		if(ProductFinder.current_slot == 1) {
			$(ProductFinder.cancel_btn).css('visibility','hidden');
			$(ProductFinder.back_btn).css('visibility','hidden');
		}
		$('#selector-widget div').each(function(i) {
			if(i >= ProductFinder.current_slot) {
				$(this).remove();
			}
		});
		$(ProductFinder.submit_btn).css('visibility','hidden');
		ProductFinder.slot_data.length = ProductFinder.current_slot;
		$('#selector-widget span.nav:eq(' + (ProductFinder.current_slot - 1) + ') a').removeClass('selected');
		$('#selector-widget div:eq(' + (ProductFinder.current_slot - 1) + ')').removeClass('clicked');
		$('#selector-widget div:eq(' + (ProductFinder.current_slot - 1) + ') img.selected').remove();
		$('input.customize-option:eq(' + (ProductFinder.current_slot - 1) + ')').val('');
		$('#selector-widget div:eq(' + (ProductFinder.current_slot - 1) + ')').append(ProductFinder.back_btn);
		return false;
	});
	//reset the values
	$(ProductFinder.cancel_btn).click(function(){
		$('#selector-widget span.nav a').removeClass('selected');
		$('#selector-widget div').removeClass('clicked');
		$('#selector-widget div img.selected').remove();
		$(ProductFinder.cancel_btn).css('visibility','hidden');
		$(ProductFinder.back_btn).css('visibility','hidden');
		$(ProductFinder.submit_btn).css('visibility','hidden');
		$('input.customize-option').val('');
		ProductFinder.num_checked = 0;
		ProductFinder.current_slot = 1;
		ProductFinder.slot_data.length = 1;
		$('#selector-widget div:not(:first)').remove();
		return false;
	});
}

//append the spinner
ProductFinder.loadSpinner = function(parent_el, action) {
	var opts = {
	  lines: 12, // The number of lines to draw
	  length: 4, // The length of each line
	  width: 2, // The line thickness
	  radius: 5, // The radius of the inner circle
	  color: '#000', // #rgb or #rrggbb
	  speed: 1, // Rounds per second
	  trail: 60, // Afterglow percentage
	  shadow: false // Whether to render a shadow
	};
	if(action == "start") {
		$(parent_el).find('form').fadeTo(0, 0.3);
		$(ProductFinder.spinner_container).css({'position' : 'absolute', 'left' : '49%', 'top' : '50%'}).spin(opts);
	} else if (action == "stop") {
		$(parent_el).find('form').fadeTo(0, 1);
		$(ProductFinder.spinner_container).spin(false);
	}
	if(!ProductFinder.setup) {
		$(parent_el).append(ProductFinder.spinner_container);
	}
}

//for building the form w/o using the js template. keeping just in case.
ProductFinder.buildSelectorForm = function () {
	curr_cat = ProductFinder.current_slot;
	var cat_html = '';
	
	cat_html += '<div id="cat-' + ProductFinder.current_slot + '">';
	cat_html += '<h2>' + ProductFinder.slot_data[ProductFinder.current_slot - 1].label + '</h2>';
	cat_html += '<p>' + ProductFinder.slot_data[ProductFinder.current_slot - 1].question + '</p>';
	cat_html += '<span class="nav">';
	$(ProductFinder.slot_data[ProductFinder.current_slot - 1].answers).each(function (j) {
		var curr_op = j + 1;
		var selected_op;
		j == 0 ? selected_op = 'selected' : selected_op = '';
		cat_html += '<a href="#cat-' + curr_cat + '-' + curr_op + '" class="cat-' + curr_cat + '-1 ' + selected_op + '" id="cat-' + curr_cat + '-1">' + this.option + '</a>';
	});
	cat_html += '</span>';
	cat_html += '<input type="hidden" name="' + ProductFinder.slot_data[ProductFinder.current_slot - 1].input_name + '" id="' + ProductFinder.slot_data[ProductFinder.current_slot - 1].input_name + '-value" class="customize-option" value="" />';
	cat_html += '<span class="img-caption" style="background-image:url(' + ProductFinder.slot_data[ProductFinder.current_slot - 1].answers[0].image + ');"></span>';
	cat_html += '</div>';
	$('#selector-widget fieldset').append(cat_html);
	ProductFinder.evenColumns($('#selector-widget div'));
	if(!ProductFinder.setup) {
		ProductFinder.productSelectorHandlers();
	}
}