var ProductFinderModal = {};
//container for load spinner
ProductFinderModal.spinner_container = document.createElement('div');

$(function () {
	ProductFinderModal.starRatings();
	ProductFinderModal.videoHandlers();
	$('div.carousel div.carousel-content').jCarouselLite({
	    btnNext: ".carousel .next",
	    btnPrev: ".carousel .prev"
	});
});

//load spinner
$.fn.spin = function(opts) {
  this.each(function() {
    var $this = $(this),
        data = $this.data();

    if (data.spinner) {
      data.spinner.stop();
      delete data.spinner;
    }
    if (opts !== false) {
      data.spinner = new Spinner($.extend({color: $this.css('color')}, opts)).spin(this);
    }
  });
  return this;
};

ProductFinderModal.videoHandlers = function () {
	//only use listeners when modal is open and there is a player window
	if($('#video-player').size() > 0) {
		$('a.play-video').click(function () {
			var vid_id = ProductFinderModal.extractVidID($(this).attr('href'));
			ProductFinderModal.videoCue(true, vid_id );
			return false;
		});
	}
}

ProductFinderModal.videoCue = function (autoplay, start_vid) {
	$('.vid-link:first a').addClass('playing');
	$("#video-player").empty();
	$("#video-player").tubeplayer({
    	width: '530px', 
    	height: '320px',
    	allowFullScreen: "true",
    	initialVideo: start_vid,
    	preferredQuality: "default",
    	autoPlay: autoplay
    });
}

ProductFinderModal.extractVidID = function (vid_link) {
	//the two most common ways people like to YouTube Videos
	var youtube_pattern_1 = /^.+v=(.[^&]+).*$/
	var youtube_pattern_2 = /^.+youtu\.be\/(.+)$/
	return vid_link.replace(youtube_pattern_1,'$1').replace(youtube_pattern_2,'$1');
}

ProductFinderModal.starRatings = function () {
	//renders stars based on em value inside markup
	$('span.star-rating').each(function() { 
		var max_stars = 5;
		if($(this).find('em').size() > 0) {
			var max_width = $(this).width();
			var star_rating = parseFloat($(this).find('em').html());
			var star_width = Math.round((star_rating / max_stars) * max_width);
			$(this).find('em').css('width',star_width + 'px');
		}
	});
}

ProductFinderModal.evenColumns = function (cols) {
	var tallest = 0;
	$(cols).each(function (i) {
		$(this).height() > tallest ? tallest = $(this).height() : '';
		i == $(cols).size() - 1 ? $(this).addClass('last') : '';
	});	
	$(cols).css('min-height',tallest);
}

//append the spinner
ProductFinderModal.loadSpinner = function(parent_el, action) {
	var opts = {
	  lines: 12, // The number of lines to draw
	  length: 4, // The length of each line
	  width: 2, // The line thickness
	  radius: 5, // The radius of the inner circle
	  color: '#000', // #rgb or #rrggbb
	  speed: 1, // Rounds per second
	  trail: 60, // Afterglow percentage
	  shadow: false // Whether to render a shadow
	};
	if(action == "start") {
		$(parent_el).find('form').fadeTo(0, 0.3);
		$(ProductFinderModal.spinner_container).css({'position' : 'absolute', 'left' : '49%', 'top' : '50%'}).spin(opts);
	} else if (action == "stop") {
		$(parent_el).find('form').fadeTo(0, 1);
		$(ProductFinderModal.spinner_container).spin(false);
	}
	if(!ProductFinderModal.setup) {
		$(parent_el).append(ProductFinderModal.spinner_container);
	}
}