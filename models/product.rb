class Product

  @sample_products = {
    'flurry-1400' => {
      'name' => 'Flurry&trade; 1400 Electric Snow Thrower',
      'description' => '14" Single-Stage Electric Snow Thrower - 11 amp Troy-Bilt&reg; engine',
      'price' => 199.99,
      'status' => 'In stock',
      'image' => '/wcpics/TroyBiltUS/en_US/images/content/flurry-1400.jpg',
      'top_rated' => false,
      'rating' => 4.7
    },
    'squall-210' => {
      'name' => 'Squall&trade; 210 Snow Thrower',
      'description' => '21" Single-Stage Snow Thrower - 123cc Troy-Bilt&reg; engine',
      'price' => 359.99,
      'status' => 'In stock',
      'image' => '/wcpics/TroyBiltUS/en_US/images/content/squall-210.jpg',
      'top_rated' => false,
      'rating' => 4.7
    },
    'squall-2100' => {
      'name' => 'Squall&trade; 2100 Snow Thrower',
      'description' => '21" Single-Stage Snow Thrower - 208cc Troy-Bilt&reg; engine',
      'price' => 499.99,
      'status' => 'In stock',
      'image' => '/wcpics/TroyBiltUS/en_US/images/content/squall-2100.jpg',
      'top_rated' => false,
      'rating' => 4.7
    },
    'storm-2410-lowes' => {
      'name' => 'Storm&trade; 2410 Snow Thrower - Lowe\'s Exclusive',
      'description' => '24" Two-Stage Snow Thrower - 179cc Troy-Bilt engine',
      'price' => 599.99,
      'status' => 'In-store only',
      'image' => '/wcpics/TroyBiltUS/en_US/images/content/storm-2410-lowes.jpg',
      'top_rated' => false,
      'rating' => 4.7
    },
    'storm-2410' => {
      'name' => 'Storm&trade; 2410 Snow Thrower',
      'description' => '24" Two-Stage Snow Thrower - 208cc Troy-Bilt engine',
      'price' => 599.99,
      'status' => 'In stock',
      'image' => '/wcpics/TroyBiltUS/en_US/images/content/storm-2410.jpg',
      'top_rated' => false,
      'rating' => 4.7
    },
    'storm-2620' => {
      'name' => 'Storm&trade; 2620 Snow Thrower',
      'description' => '26" Two-Stage Snow Thrower - 208cc Troy-Bilt engine',
      'price' => 759.99,
      'status' => 'In stock',
      'image' => '/wcpics/TroyBiltUS/en_US/images/content/storm-2620.jpg',
      'top_rated' => false,
      'rating' => 4.7
    },
    'storm-2840' => {
      'name' => 'Storm&trade; 2840 Snow Thrower',
      'description' => '28" Deluxe Two-Stage Snow Thrower - 277cc Troy-Bilt engine',
      'price' => 899.99,
      'status' => 'In stock',
      'image' => '/wcpics/TroyBiltUS/en_US/images/content/storm-2840.jpg',
      'top_rated' => true,
      'rating' => 4.7
    },
    'storm-tracker' => {
      'name' => 'Storm Tracker&trade; 2690 XP Snow Thrower',
      'description' => 'Storm Tracker&trade; 2690 XP Snow Thrower<br />26" Deluxe Two-Stage Snow Thrower - Just One Touch&trade;<br />Electric Chute Control - 208cc Troy-Bilt engine',
      'price' => 1099.99,
      'status' => 'In stock',
      'image' => '/wcpics/TroyBiltUS/en_US/images/content/storm-tracker.jpg',
      'top_rated' => false,
      'rating' => 4.7
    },
    'storm-3090-lowes' => {
      'name' => 'Storm&trade; 3090 XP Snow Thrower - Lowe\'s Exclusive',
      'description' => '30" Deluxe Two-Stage Snow Thrower - Just One Touchtrade; Electric Chute Control - 357cc Troy-Bilt engine',
      'price' => 1199.99,
      'status' => 'In-store only',
      'image' => '/wcpics/TroyBiltUS/en_US/images/content/storm-3090-lowes.jpg',
      'top_rated' => false,
      'rating' => 4.7
    },
    'storm-3090' => {
      'name' => 'Storm&trade; 3090 Snow Thrower',
      'description' => '30" Deluxe Two-Stage Snow Thrower - 357cc Troy-Bilt&reg; engine',
      'price' => 1099.99,
      'status' => 'In stock',
      'image' => '/wcpics/TroyBiltUS/en_US/images/content/storm-3090.jpg',
      'top_rated' => true,
      'rating' => 4.7
    },
    'polar-3310' => {
      'name' => 'Polar Blast&trade; 3310 XP Snow Thrower',
      'description' => '33" Two-Stage Snow Thrower - 357cc Troy-Bilt&reg; engine',
      'price' => 1799.99,
      'status' => 'In stock',
      'image' => '/wcpics/TroyBiltUS/en_US/images/content/polar-3310.jpg',
      'top_rated' => false,
      'rating' => 4.7
    },
    'polar-4510' => {
      'name' => 'Polar Blast&trade; 4510 XP Snow Thrower',
      'description' => '45" Deluxe Two-Stage Snow Thrower - 420cc Troy-Bilt&reg; engine',
      'price' => 2299.99,
      'status' => 'In stock',
      'image' => '/wcpics/TroyBiltUS/en_US/images/content/polar-4510.jpg',
      'top_rated' => false,
      'rating' => 4.7
    }
  }

  def self.get_sample
    @sample_products
  end

end